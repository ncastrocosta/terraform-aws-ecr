# terraform-aws-ecr

Terraform module for AWS ECR.

## Running the examples

For the examples, you must specify the AWS profile.

```bash
$ AWS_PROFILE=<profile> terraform init|plan|apply
```
